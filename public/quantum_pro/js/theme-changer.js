$(function(){
  var theme = localStorage.currentTheme;
  if(theme){
    $("body").removeClass();
    $("body").addClass(theme);
  } else {
    $("body").removeClass();
    $("body").addClass("skin-blue");
  }

  $("#palette-light-black").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-black");
    localStorage.currentTheme = "skin-black";
  })
  $("#palette-light-green").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-green");
    localStorage.currentTheme = "skin-green";
  })
  $("#palette-light-blue").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-blue");
    localStorage.currentTheme = "skin-blue";
  })
  $("#palette-light-purple").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-purple");
    localStorage.currentTheme = "skin-purple";
  })
  $("#palette-dark-white").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-dark-white");
    localStorage.currentTheme = "skin-dark-white";
  })
  $("#palette-dark-green").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-dark-green");
    localStorage.currentTheme = "skin-dark-green";
  })
  $("#palette-dark-blue").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-dark-blue");
    localStorage.currentTheme = "skin-dark-blue";
  })
  $("#palette-dark-purple").click( function() {
    $("body").removeClass();
    $("body").addClass("skin-dark-purple");
    localStorage.currentTheme = "skin-dark-purple";
  })
  
});